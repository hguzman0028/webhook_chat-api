import pymongo
import re
import datetime
import sys
from bson import ObjectId
import os
#Clase de base de datos
class DB:

    def __init__(self,con):
        self.con = con
        self.var_ID_exist_conversacion= None
        self.var_check_last_conversacion= None
        self.var_ID_crear_conversacion=None
        self.var_check_message_unsend=None
        self.var_check_last_message_in= None
        self.var_check_lasts_message_in_db=None
        self.var_check_last_photo_in=None
        self.var_update_state_msg_unsend=None
        self.var_check_lasts_photo_in=None
        self.con_monitoreo=None
        self.var_check_last_conversacion_id=None
        self.var_check_lasts_video_in = None
        self.var_check_lasts_docs_in = None
        self.var_check_lasts_audio_in = None
        self.var_check_file_unsend = None
        
   
    def exist_conversacion(self,origen): 
        print("exist_conversacion")
        #self.conect()
        db = self.con
        col = db['conversaciones']

        query={"origen":origen,"estado":{"$in":["ATENDIENDO","NO_ATENDIDO"]},"estadoBot":{"$in":["ATENDIENDO","NO_ATENDIDO","ESCALADO","JALADO_X_AGENTE"]} }

        result = col.find_one(query,{"_id":1})
        

        if db.conversaciones.count_documents(query) != 0:
            self.var_ID_exist_conversacion = result['_id']
            #return result['_id']
        else:
            self.var_ID_exist_conversacion=None

    def crear_conversacion(self,origen,canal,API): 
        print("crear_conversacion")
        #self.conect()
        db = self.con
        col = db['conversaciones']

        #query={"origen":origen,"estado":"NO_ATENDIDO","canal":canal,"API":API,"categoria":ObjectId(os.environ.get('categoria')),"fecha":datetime.datetime.now(),"tipo":"inbound"}
        query={"origen":origen,"estado":"NO_ATENDIDO","estadoBot":"NO_ATENDIDO","canal":canal,"API":API,"categoria":ObjectId(os.environ.get('categoria')),"fecha":datetime.datetime.now(),"fechaAtendido":datetime.datetime.now(),"agente":ObjectId('5f1a10818cb33b3b38acd401'),"encuestaAgenteStatus":"False","tipo":"inbound","idx_estado":{"nombre":"NO_ATENDIDO","clave":0} } 

        col.insert_one(query)

        self.exist_conversacion(origen)

        #return id
        self.var_ID_crear_conversacion = self.var_ID_exist_conversacion



    def insert_chat(self,mensaje,id,hora,id_msg,type_messege,channelId,platform,caption):
        print("insert_chat "+str(type_messege)+":"+str(mensaje))
        #self.conect()
        db = self.con

        fecha_dt = datetime.datetime.now()

        col = db['mensajes']
        try:

            check_file = re.search("^http", str(mensaje))

            if check_file: 
                query={"_id":ObjectId(id_msg),"rol":"cliente","file":mensaje,"texto":caption,"conversacion":id,"estado":"NO_VISTO","fecha":fecha_dt,"canal":platform,"tipo":type_messege,"channelIdAPI":channelId}  
                col.insert_one(query)
            else:
                query={"_id":ObjectId(id_msg),"rol":"cliente","texto":mensaje,"conversacion":id,"estado":"NO_VISTO","fecha":fecha_dt,"canal":platform,"tipo":type_messege,"channelIdAPI":channelId}  
                col.insert_one(query) 

        except:
            print("Unexpected error:", sys.exc_info()[0]) 


        
        

    def insert_chatB(self,mensaje,id,hora,id_msg,type_messege,channelId,platform,caption):
        print("insert_chat "+str(type_messege)+":"+str(mensaje))
        #self.conect()
        db = self.con

        fecha_dt = datetime.datetime.now()

        col = db['mensajes']
        try:

            check_file = re.search("^http", str(mensaje))

            #if caption == "sin_caption":

            if check_file: 
                query={"rol":"cliente","file":mensaje,"texto":caption,"conversacion":id,"estado":"NO_VISTO","fecha":fecha_dt,"canal":platform,"tipo":type_messege,"channelIdAPI":channelId}  
                col.insert_one(query)
            else:
                query={"rol":"cliente","texto":mensaje,"conversacion":id,"estado":"NO_VISTO","fecha":fecha_dt,"canal":platform,"tipo":type_messege,"channelIdAPI":channelId}  
                col.insert_one(query)
            #else:
                #query={"_id":id_msg,"rol":"cliente","file":mensaje,"texto":caption,"conversacion":id,"estado":"NO_VISTO","fecha":fecha_dt,"canal":platform,"tipo":type_messege,"channelIdAPI":channelId}  
                #col.insert_one(query) 

        except:
            print("Unexpected error:", sys.exc_info()[0])
     
