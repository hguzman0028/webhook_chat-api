import datetime
import sys
import json
import pymongo
import re
import datetime
import sys
from bson import ObjectId
import conversaciones
import os

#Conec#####
client = pymongo.MongoClient(os.environ.get('conexion'))
db = client[os.environ.get('db')]
#Conec#####

db = conversaciones.DB(db)

def lambda_handler(event, context):
                         
    
    try:

        type_messege= event['messages'][0]['type']
        messege_in = None
        author = str(event['messages'][0]['author']).split("@")
        origen= author[0]
        
        

        if event['messages'][0]['fromMe'] == False: 

            if type_messege=='chat':
                
                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']
                registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,"text",channelId,"whatsapp",None)
                return {"registro":"Exitoso "+str(type_messege)} 

            elif type_messege=='image':

                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']

                try:
                    text_con_img = event['messages'][0]['caption'] 
                    registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,type_messege,channelId,"whatsapp",text_con_img) 
                    return {"registro":"Exitoso "+str(type_messege)} 
                except:
                    text_con_img = "" 
                    registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,type_messege,channelId,"whatsapp",text_con_img)
                    return {"registro":"Exitoso "+str(type_messege)} 


            elif type_messege=='video':

                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']

                try:
                    text_con_img = event['messages'][0]['caption'] 
                    registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,type_messege,channelId,"whatsapp",text_con_img) 
                    return {"registro":"Exitoso "+str(type_messege)} 
                except:
                    text_con_img = "" 
                    registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,type_messege,channelId,"whatsapp",text_con_img)
                    return {"registro":"Exitoso "+str(type_messege)} 
            elif type_messege=='document':
                
                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']
                registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,"file",channelId,"whatsapp",None)
                return {"registro":"Exitoso "+str(type_messege)} 
            elif type_messege=='ptt': 
                
                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']
                registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,"audio",channelId,"whatsapp",None)
                return {"registro":"Exitoso "+str(type_messege)} 
            elif type_messege=='location':
                
                messege_in = event['messages'][0]['body']
                #id_msg = event['message']['id']
                channelId = event['messages'][0]['chatId']
                #platform = event['message']['platform']
                registrar_mensajeB(origen,messege_in,datetime.datetime.now(),None,"location",channelId,"whatsapp",None)  
                return {"registro":"Exitoso "+str(type_messege)} 
                

            else: 
                return {"respuesta":"Tipo de archivo no encontrado"}    
        else:
            return {"respuesta":"Fail fromMe"}
    except:
        return {"registro":"Fallido","conversacion":""+str(sys.exc_info())}



def registrar_mensajeB(origen,messege_in,fecha,id_msg,type_messege,channelId,platform,caption):
    db.exist_conversacion(str(origen)) 
    print(origen)
    print(db.var_ID_exist_conversacion)

    if db.var_ID_exist_conversacion:
            
        db.insert_chatB(messege_in,db.var_ID_exist_conversacion,fecha,id_msg,type_messege,channelId,platform,caption)
        return {"registro":"exitoso","conversacion":"conocida","ID":str(db.var_ID_exist_conversacion)}
    else:
        db.crear_conversacion(str(origen),"whatsapp",os.environ.get('API'))  
        db.insert_chatB(messege_in,db.var_ID_crear_conversacion,fecha,id_msg,type_messege,channelId,platform,caption) 
        return {"registro":"exitoso","conversacion":"nueva","ID":str(db.var_ID_crear_conversacion)}
